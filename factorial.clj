;; factorial.clj

(defn factorial [num]
  (if (zero? num) 1
      (* num (factorial (dec num)))))

(factorial 0) ; => 1
(factorial 1) ; => 1
(factorial 3) ; => 6
(factorial 7) ; => 5040
