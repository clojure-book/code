"A string"

(type "A string")

#"regexp"

(type #"regexp")

(re-matches #"\d" "There is no number here.")

(re-matches #"\d" "4")

(re-matches #"\d" "42")

(re-matches #"\d+" "42")

(re-matches #"\d+" "42 is the ultimate answer.")

(re-find #"\d+" "42 is the ultimate answer.")

(re-find #"\d+" "42 is the ultimate answer, and so is 52.")

(re-find #"\d+" "Forty two is the ultimate answer, and so is 52.")

(re-seq #"\d+" "42 is the ultimate answer, and so is 52.")

(re-seq #"(?i)abc" "abc are small letters and ABC are capitals.")

(re-seq #"abc" "abc are small letters and ABC are capitals.")

(re-seq #"(?i)ABC" "abc are small letters and ABC are capitals.")

(re-seq #"(?i)abC" "abc are small letters and ABC are capitals.")

(re-seq #"[A-Z]+" "Finds all CAPITAL letter WORDS.")

(require '[clojure.string :as str])

(str/replace "There    are     lots of   spaces" #"\s+" " ")

(str/split "There   are     lots of   spaces" #"\s")

(str/split "There   are     lots of   spaces" #"\s+")
