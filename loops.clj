;; loops.clj

(let [nums [1 2 3 4]]
  (for [num nums]
       (* num 10)))

(def colors ["red" "blue" "green" "yellow"])

(def shapes ["square" "circle" "triangle" "rectangle"])

(for [color colors
      shape shapes]
  (str color " " shape))

(let [nums [1 2 3 4]]
  (doseq [num nums]
    (* num 10)))

(let [nums [1 2 3 4]]
  (doseq [num nums]
    (println (* num 10))))

(loop [x 1]
  (when (<= x 5)
    (println x)
    (recur (inc x))))

(dotimes [x 5]
  (println x))

(dotimes [_ 5]
  (println "Hello World!"))
