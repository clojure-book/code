;; what_it_returns_2.clj

(defn do-math [a b]
  (* a b)
  (+ a b))

(do-math 5 3)
