(try
  (slurp "does_not_exist.txt")
  (catch Exception e (println (str "caught exception: " (.getMessage e)))))

(try
  (slurp "does_not_exist.txt")
  (println "this won't be printed")
  (catch Exception e (println (str "caught exception: " (.getMessage e)))))

(try
  (slurp "does_not_exist.txt")
  (println "this won't be printed")
  (catch Exception e (println (str "caught exception: " (.getMessage e))))
  (finally (println "code something that will deal with this exception")))

(try
  (slurp "does_not_exist.txt")
  (println "this won't be printed")
  (catch java.io.FileNotFoundException e (println (str "caught exception: " (.getMessage e))))
  (finally (println "code something that will deal with this exception")))

(try
  ;; Code that might throw an exception
  (let [x (Integer/parseInt "not-a-number")]
    (println x))

  (catch NumberFormatException e
    (println "Caught a NumberFormatException:" (.getMessage e)))

  (catch NullPointerException e
    (println "Caught a NullPointerException:" (.getMessage e)))

  (catch Exception e
    (println "Caught a generic exception:" (.getMessage e))))


(defn print-length [s]
  (println "Length of the string is:" (.length s)))

;; Calling the function with a non-null string
(print-length "Hello, World!") ;; Works fine

;; Calling the function with nil
(print-length nil) 

(try
  ;; Code that might throw an exception
  (print-length nil)

  (catch NumberFormatException e
    (println "Caught a NumberFormatException:" (.getMessage e)))

  (catch NullPointerException e
    (println "Caught a NullPointerException:" (.getMessage e)))

  (catch Exception e
    (println "Caught a generic exception:" (.getMessage e))))
