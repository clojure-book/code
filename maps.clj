;; maps.clj

{"name" "Bashir" "age" 12}

(hash-map "name" "Bashir"
          "age" 20)

(def friend {"name" "Bashir" "age" 12})

(get friend "name")

(friend "name")

(type "name")

(type :name)

(def wise-friend {:name "Periyaar"
                  :age 90})

(wise-friend :name)

(get wise-friend :age)

(print (wise-friend :name)
       "is very wise.")

(:name wise-friend)

(assoc wise-friend :belief "Rationalism")

(wise-friend :belief)

(wise-friend :age)

(dissoc wise-friend :age)

(keys wise-friend)

(vals wise-friend)

(assoc wise-friend "name" "Ramasamy")
