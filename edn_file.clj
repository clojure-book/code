;; edn_file.clj

(require '[clojure.edn :as edn])

(def spell-numbers
  (edn/read-string (slurp "spell_numbers.edn")))

(println spell-numbers)

(spit "spell_numbers.edn" (prn-str (assoc spell-numbers 4 "Four")))

(def spell-numbers-appended
  (edn/read-string (slurp "spell_numbers.edn")))

(println spell-numbers-appended)

(spit "spell_numbers.edn" (prn-str spell-numbers))
