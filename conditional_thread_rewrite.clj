(let [a 11]
  (cond
    (odd? a) (conj [] (* a 2))
    (even? a) (conj [] (/ a 2))))


(let [a 10]
  (cond
    (odd? a) (* a 2 1)
    (even? a) (/ a 2 1)))

