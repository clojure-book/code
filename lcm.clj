;; lcm.clj

(load-file "gcd_function.clj")

(defn lcm [first-number second-number]
  (/ (* first-number second-number)
     (gcd first-number second-number)))

(println (lcm 12 14))
