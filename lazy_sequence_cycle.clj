;; lazy_sequence_cycle.clj

(def multiverse (cycle ["universe" "antiverse"]))

(first multiverse)

(second multiverse)

(take 5 multiverse)

(nth multiverse 2000)

(nth multiverse 1783)

(def tiffin-items
  (cycle ["idli", "vadai", "dosai", "sambar"]))

(nth tiffin-items 5)

(take 10 tiffin-items)
