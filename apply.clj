;; apply.clj

(+ 1 2 3 4)

(+ [1 2 3 4]) ;; error

(apply + [1 2 3 4])

(apply + '(1 2 3 4))

(defn my-function [arg1 arg2]
  (println "Argument 1:" arg1)
  (println "Argument 2:" arg2))

(my-function "one" "two")

(my-function ["one" "two"]) ;; error

(apply my-function ["one" "two"])
