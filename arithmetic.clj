;; arithmetic.clj

(+ 40 2)

(- 44 2)

(* 6 7)

(/ 210 5)

(+ 1 2 3 4 5)

(/ (+ 1 2 3 4 5) 5)

(/ 1 2 3) ; this gives out a ratio, a clojure data type

(/ 1 6.0)

(/ 1 2.0 3)

(/ 1 2 3.0)

(quot 14 5)

(rem 14 5)

;; Let's calculate area of circle of radius 7 units
(* (/ 22 7) (* 7 7))

(* (/ 22 7) (* 7 7.0))

(* Math/PI (* 7 7))

(* Math/PI (Math/pow 7 2))
