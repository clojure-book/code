;; gcd-function.clj

(defn gcd [first-number second-number]
  (let [bigger-number (max first-number second-number)
        smaller-number (min first-number second-number)
        remainder (rem bigger-number smaller-number)]
    (if (= remainder 0)
      smaller-number
      (recur smaller-number remainder))))
