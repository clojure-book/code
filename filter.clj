(filter odd? (range 1 11))

(filter even? (range 1 11))

(filter #(> % 5) (range 1 11))

(filter #(> 5 %) (range 1 11))

(filter (fn [x] (> x 5)) (range 1 11))

(defn greater-than-5 [x]
  (> x 5))

(filter greater-than-5 (range 1 11))
