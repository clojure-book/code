;; let.clj

(def my-name "Karthikeyan")
(def initials "A K")

(let [full-name (str initials " " my-name)]
  (println full-name)
  (println my-name))

(println my-name)
(println full-name)
