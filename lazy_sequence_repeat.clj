;; lazy_sequence_repeat.clj

(def multiverse (repeat "universe"))

(first multiverse)

(take 5 multiverse)

;; (rest multiverse) ;; doesn't seem to work

;; (last multiverse) ;; doesn't seem to work

(nth multiverse 10,000)

(nth multiverse 100)

(repeat 5 "universe")

(nth (repeat 5 "universe") 6)

(nth (repeat 5 "universe") 3)

(rest (repeat 5 "universe"))

(last (repeat 5 "universe"))
