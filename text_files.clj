;; text_files.clj

(spit "hello.txt" "Hello World!")

(println (slurp "hello.txt"))

;; appending content to files

(spit "hello.txt" "\nHello Mars!" :append true)

(println (slurp "hello.txt"))

;; deleting files

(require '[clojure.java.io :as io])

(io/delete-file "hello.txt")
