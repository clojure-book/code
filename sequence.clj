;; sequence.clj

(count '(1 2 3 4))

(count [1 2 3 4])

(count #{1 2 3 4})

(count {:one 1 :two 2 :three 3 :four 4})

(seq '(1 2 3 4))

(seq [1 2 3 4])

(seq #{1 2 3 4})

(seq {:one 1 :two 2 :three 3 :four 4})

(count '(1 2 3 4))

(count '([:one 1] [:two 2] [:three 3] [:four 4]))

;; additional examples

(seq [1 2 3])

(seq '(1 2 3))

(seq #{1 2 3})

(seq {1 "one" 2 "two" 3 "three"})

(partition 2 {1 "one" 2 "two" 3 "three" 4 "four"})

(partition 2 (seq {1 "one" 2 "two" 3 "three" 4 "four"}))

(partition 2 '(1 2 3 4))

(partition 2 (seq '(1 2 3 4)))

(partition 2 #{1 2 3 4})

(partition 2 (seq #{1 2 3 4}))

(partition 2 [1 2 3 4])

(partition 2 (seq [1 2 3 4]))
