'(1 2 3 4)

(list 1 2 3)

(list 1 2 3)

(def friends-list
    '("Ram" "Bashir" "Antony" "Buddha"))

(count friends-list)

(first friends-list)

(rest friends-list)

(nth friends-list 3)

(friends-list 3)

(conj friends-list "Periyaar")
