;; printing_things.clj

(println "Hello World!")

(println "Hello World!") ; Says Hello to this world

;; This program says hello to this world
(println "Hello World!")

(println "Hello World!" "Try staying cool.")
