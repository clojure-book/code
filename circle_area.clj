;; circle_area.clj

(def radius 7)

(* Math/PI (Math/pow radius 2))
