;; threading.clj

;; thread first
(-> 5
    (Math/pow 2))

(-> 5
    (Math/pow 2)
    inc)

(-> 10
    inc
    (* 2)
    (+ 5))

;; thread last
(->> 5
    (Math/pow 2))

(->> 5
     (Math/pow 2)
     inc)

;; thread as
(as-> 5 x
  (Math/pow x 2))

(as-> 5 x
  (Math/pow 2 x))

;; conditional threading
(let [a 11]
  (cond-> []
   (odd? a) (conj (* a 2))
   (even? a) (conj (/ a 2))))

(let [a 10]
  (cond->> 1
   (odd? a) (* a 2)
   (even? a) (/ a 2)))
