;; sets.clj

#{1 2 3 4}

(type #{1 2 3 4})

(set? #{1 2 3 4})

#{1 2 3 4 4}

#{"Apple" "Orange" "Mango" "Banana"}

#{"Apple" "Orange" "Mango" "Banana" "Apple"}

(def fruits #{"Apple" "Orange" "Mango" "Banana"})

(contains? fruits "Banana")

(contains? fruits "Jack Fruit")

(fruits "Banana")

(fruits "Jack Fruit")

("Banana" fruits)

(def programming-languages #{:ruby :python :clojure})

(contains? programming-languages :ruby)

(contains? programming-languages :java)

(programming-languages :ruby)

(:ruby programming-languages)

(conj programming-languages :perl)

(disj programming-languages :python)
