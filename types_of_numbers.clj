;; types_of_numbers.clj

(type 147)

(type 147.67)

(type Math/PI)

(type (/ 84 32))

(/ 84 32)

(type 32)

(double 32)

(type (double 32))

(/ 84 (double 32))

(type (/ 84 (double 32)))

(long 42.32)

(type (long 42.32))

