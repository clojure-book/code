;; vectors.clj

[1 2 3 4] ; a very simple vector

(vector 1 2 3)

[1 true "Bashir"] ; vector containing multiple data types

(def friends
  ["Ram" "Bashir" "Antony" "Buddha"])

(first friends)

(rest friends)

(type friends)

(type (rest friends))

(nth friends 3)

(friends 3)

(conj friends "Periyaar")

(println friends)

(cons "Periyaar" friends)
