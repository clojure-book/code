;; variables.clj

(def my-name "Karthik")

(println my-name)

(println "Hello" my-name)

(def greeting (str "Hello " my-name "!"))

(println greeting)

(let [local-variable "something"]
  (println local-variable))

(println local-variable)
