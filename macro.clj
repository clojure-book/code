;; macro.clj

(defmacro calculate [a-list]
  (list ; convert (2 + 3) to (+ 2  3)
   (second a-list)
   (first a-list)
   (last a-list)))

(println
 (calculate (2 + 3)))

(println
 (macroexpand
  '(calculate (2 + 3))))
