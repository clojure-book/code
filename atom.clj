;; atom.clj

(def counter (atom 0))

@counter

counter

(swap! counter inc)

@counter

(reset! counter 42)

@counter

(def robo-count (atom 0))

(defn make-robot []
  (swap! robo-count inc)
  (str "Made a robot. Total robots: " @robo-count))

(make-robot)
(make-robot)
(make-robot)

(println (str "Robots made: " @robo-count))
