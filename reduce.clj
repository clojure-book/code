 ;; reduce.clj

(reduce + [1 2 3 4])

(reduce * [1 2 3 4])

(defn add [a b]
  (+ a b))

(reduce add [1 2 3 4])

(defn multiply [a b]
  (* a b))

(reduce multiply [1 2 3 4])


