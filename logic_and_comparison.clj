;; logic-and-comparison.clj

(= 1 1)

(= 1 2)

(= 7 7 7 )

(= 7 8 7)

(> 1 2)

(> 2 1)

(> 3 2 1)

(>= 5 5)

(>= 6 5)

(>= 6 7)

(< 1 2)

(< 1 2 3)

(< 2 1)

(< 1 3 2)

(<= 7 7)

(<= 7 8)

(<= 8 7)

(and true true)

(and  true false)

(or true true)

(or true false)

(or false false)

(not true)

(not false)
